# Descriptif du projet
**Auteur** : Lekhlaikh Taha
**Contact**: taha.lekhlaikh@imt-atlantique.net
**Formation** : Master Spécialisé Infrastructures Cloud et DevOps

J'ai choisi d'utiliser deux dépôt GitLab dédié, un pour docker et second pour Terraform et Ansible.
https://gitlab.imt-atlantique.fr/t22lekhl/prod-ci.git
https://gitlab.imt-atlantique.fr/t22lekhl/fil_rouge_4
Cela m’a permis de générer automatiquement l'architecture ainsi que le déploiement de l'application après le lancement du pipeline.

# Introduction

L'objectif de ce fil rouge est d’automatisé le processus de déploiement de l'application Vapormap sur une instance déployée dans l'infrastructure Openstack. Pour ce faire, nous allons devoir mettre en place une infrastructure en tant que code à l'aide de Terraform, une fois l’infrastructure provisionnée, nous pouvons utiliser Ansible pour déployer et configurer notre application sur l'infrastructure que nous avons créée avec Terraform, nous pouvons écrire des playbooks Ansible pour automatiser le déploiement et la configuration de notre application. Nous allons utiliser ensuite GitLab CI pour automatiser le processus de déploiement et de configuration de notre infrastructure et de notre application en créant un pipeline CI/CD dans GitLab CI qui comprend les étapes (stage) suivantes :

**INIT:**  Elle vérifie la configuration en recherchant les erreurs de syntaxe dans la configuration Terraform et s'assure que tous les paramètres requis sont présents et  initialise le répertoire de travail de Terraform et installe les plugins pour gérer les ressources d'infrastructure pour le fournisseur Openstack.

**PLAN :** Ce stage permet de prévisualiser les changements qui seront apportés à l’infrastructure, il donne ainsi l'opportunité de les revoir et de les vérifier avant leur application. Le plan nous aider à identifier les problèmes ou les erreurs qui pourraient survenir lors des changements involontaires ou de casser l'infrastructure existante.

**APPLY :** Ce stage permet de garantir que l’infrastructure est déployée de manière cohérente.

**DESTROY :** Ce stage permet de détruire l'architecture.

**DEPLOY :** Automatiser le déploiement et la gestion de l'infrastructure et de l’application, tout en assurant la cohérence et l'idempotence.


# Travail réalisé :


## Déploiement en IaC


J’ai tout d’abord commencé par déployer une infrastructure grâce à l’outil Terraform sur Openstack, cette infrastructure est composée :

-  D’une instance nommée « node » qui est attachée à un réseau qui représente une infrastructure réseau virtualisée au sein de l’environnement OpenStack.
- D’un sous-réseau qui est une section partitionnée du réseau et qui permet un contrôle plus granulaire de l'adressage et du routage IP.

- D’un routeur qui fournit un mécanisme pour transmettre le trafic entre le sous-réseau et entre l’environnement OpenStack et le réseau externe.

- D’un groupe de sécurité qui est un ensemble de règles de pare-feu qui contrôlent le trafic vers et depuis l’instance. J’ai restreint le trafic aux ports « 8000 » et « 5000 » et j’ai autorisé le trafic SSH entrant sur le port 22 à partir de toutes les adresses IP.

- D’une clé paire qui est généré automatiquement en même temps que l'infrastructure qui permet d’accéder à l'infrastructure déployée. Cela m’a permis d'éviter de créer manuellement un fichier contenant les clés ssh (C’est une remarque de notre encadrant M. Braux).

- D’une adresse IP flottante qui est une adresse IP publique qui est associée à l’instance, lui permettant d'être accessible depuis Internet, En Utilisant un client SSH on peut se connecter à l'instance en utilisant l'adresse IP flottante.

- D'un fichier "host.ini" est utilisé pour définir l'inventaire des hosts qu'Ansible va gérer qui contient une liste d'hosts cibles sur lesquels Ansible exécutera des tâches,

- D'un fichier "ip.txt" contenant l’adresse IP flottante qui sera utilisé pour déploiement l’application avec docker au lieu d’utiliser « localhost ».




J’ai utilisé ensuite Ansible pour automatiser le déploiement de l’environnement Docker à partir des images génère dans le Container Registry et au fichier docker-compose qui se trouvent dans le dépôt Gitlab du fil rouge N°3. J’ai écrit un playbook Ansible qui définit les tâches suivantes :

- J'ai tout d'abord commencé par mette en place une tâche de mise à jour du système en l’utilisant du le module « apt » qui est utilisé pour gérer les packages sur les systèmes basés sur Ubuntu, j’ai utilisé le paramètre "update_cache" pour mettre à jour le cache des packages, qui récupère les dernières informations sur les package disponibles. Ceci est important pour garantir que le système exécute les packages les plus récents et les plus sûrs.

- J’ai installé le package git à l'aide du module « apt ». Le paramètre become : true est utilisé pour élever les privilèges, permettant aux tâches d'être exécutées avec les permissions de root.

- L'application Vapormap étant sur un dépôt GitLab, j'ai effectué une tâche de clone du dépôt du fil rouge N°3 en spécifiant le répertoire de destination "/home/ubuntu/vappormap" sur les hôtes cibles.

- J’ai utilisé après le module « apt » fourni par Ansible pour installer le paquet docker-compose.

- J’ai utilisé le module shell fourni par Ansible pour lancer Docker Compose avec la commande up, en utilisant l'option -d pour une exécution en mode détaché. La commande définit également la variable d'environnement MY_IP (est utilisé dans le docker compose) à la valeur de {{ MY }}, qui une variable qui va être définie ailleurs dans la l’exécution de playbook. La section args spécifie le répertoire dans lequel j’ai exécuté la commande à l'aide du paramètre chdir.

## Utilisation d'une chaîne de Déploiement

J’ai utilisé après GitLab CI pour automatiser le processus de déploiement et de configuration de notre infrastructure et de notre application en créant un pipeline CI/CD dans GitLab CI. Pour ce faire, j’ai créé un fichier gitlab-ci.yml à la racine de notre projet. La chaîne d'intégration continue va se faire en 4 étapes (stage) et ils sont nommés : "init", "plan", "apply" et "deploy" dans le fichier gitlab-ci.yml.
Avant de mettre en place le stage « init », j’ai commencé par ajouter l’image de la dernière version de l'outil Terraform d'HashiCorp qui sera utilisé dans trois stage. Pour qu’on puisse s'authentifier et interagir avec l'API OpenStack, avant d'exécuter les scripts qui se trouvent dans les jobs j’ai défini des variables d'environnement pour le client avant d'exécuter les scripts qui se trouvent dans les jobs en utilisant des variables que j’ai stocké dans le projet car il contient des informations sensibles tel que le mot de passe.

- J’ai créé un job « init-job » pour le stage « init » qui contient une seule commande, qui est terraform init -input=false, Cette commande exécute la commande terraform init avec l'option -input=false, ce qui signifie que Terraform ne me demandera pas à saisir des données pendant le processus d'initialisation. J’ai défini les répertoires ./.terraform/ et ./.terraform.lock.hcl/en utilisant les artéfacts. Ces répertoires contiennent les fichiers dont Terraform a besoin pour s'exécuter, et ils seront transmis aux jobs suivants dans le pipeline.

- Le job « init-plan » exécute la commande terraform plan pour générer un plan des modifications à apporter à l'infrastructure. Il enregistre le plan généré dans un fichier nommé "tfplan". Il enregistre l'état actuel de la configuration Terraform dans un fichier nommé "terraform.tfstate". Les deux fichiers "tfplan" et "terraform.tfstate" sont enregistrés comme artefacts de ce job. Ce job permet de prévisualiser les changements qui seront apportés à l’infrastructure, il donne ainsi l'opportunité de les revoir et de les vérifier avant leur application. Le plan nous aider à identifier les problèmes ou les erreurs qui pourraient survenir lors des changements involontaires ou de casser l'infrastructure existante.

- Le job « apply-plan » exécute la commande terraform apply pour appliquer les changements à l'infrastructure en utilisant le plan précédemment généré dans le fichier "tfplan", en ajoutant l'option "-input=false" qui indique à Terraform de ne pas demander de confirmation avant d'appliquer les changements, ce qui est utile pour notre pipeline automatisé. Ce job enregistre le fichier "tfplan" en tant qu'artefact, indiquant que c'est le plan qui a été appliqué, Il enregistre également plusieurs autres fichiers en tant qu'artefacts, notamment le fichier d'inventaire Ansible (host.ini), un fichier de clé privée (ip.txt). L'objectif de cette tâche est de créé l'infrastructure en utilisant le plan Terraform généré à l'étape précédente " plan ". Il enregistre donc les fichiers nécessaires en tant qu'artefacts, qui vont être utilisés dans le dernier job.

- J'ai ajouté un job destroy à activer manuellement afin de détruire l'architecture en utilisant la command terraform destroy avec L'indicateur -state=terraform.tfstate spécifie le chemin d'accès au fichier d'état Terraform, qui est utilisé pour garder la trace des ressources créées par Terraform

- J'ai ensuite créé un job pour lancer mon playbook Ansible qui va déployer l'application Vapormap sur l’instance « node » à l’aide d’une commande, l'option --extra-vars est utilisée dans la commande pour transmettre l'adresse IP en tant que variable nommée MY obtenu par un fichier texte qui était généré dans le job précédent. J'ai mis en place un runner shell sur ma VDI pour exécuter ce job. Avant d’exécuter le script, j'ai réalisé diverses étapes de préparation : L’installation d’ansible, permissions nécessaires pour les fichiers (le fichier "hosts.ini" généré précédent dans le répertoire de Terraform, qui me permet de se connecter en ssh en utilisant le fichier "mykey.pem" qui contient la clé privée)

Une fois Vapormap déployé, j'ai vérifié son bon fonctionnement. Tout d'abord en m'assurant que mes trois conteneur (MariaDB, Frontend et API) étaient bien en cours d'exécution dans la machine virtuelle créée. Ensuite, j'ai lancé une commande curl sur les deux adresses de l'application, le Frontend et l'API.
# Difficultés rencontrées :


Durant la réalisation de ce projet, j'ai rencontré plusieurs problèmes. Tout d'abord, l’utilisation de l’adresse IP génère à chaque création de l’infrastructure, cette adresse doit être mis dans la variable d’environnement « VAPORMAP_BACKEND ». Pour cela, j’ai travaillé avec mon camarade Zakaria afin qu’on puisse utiliser une variable « MY_IP » qui va être utilisé dans le fichier docker-compose et déclaré dans une commande dans le playbook d’ansible et ensuite déclaré dans une commande terraform sur notre pipeline. Cette variable va prendre la valeur de l’adresse IP flottant générer et stocker dans un fichier texte « ip.txt ».

La deuxième difficulté était dans la tâche « deploy-job », lorsque je voulais exécuter la commande de ploiement je reçoi une erreur qui m’indique que la connexion SSH à l'hôte avec l'adresse IP sur le port 22 a été refusée, ce qui signifie que la connexion n'a pas pu être établie. J’ai tous d’abord vérifier que le groupe de sécurité autorise bien la connections ssh, ensuite j’ai réexécute la commande en local et j’ai remarqué qu’après l’exécution de la commande je dois confirmer la connexion d'hôte SSH dans Ansible. Pour résoudre ce problème j’ai défini une variable d'environnement qui indique à Ansible d'ignorer la vérification de la clé d'hôte SSH pour toutes les connexions suivantes [ ]. Mais au fur et à mesure que j’avance sur mon pipeline je reçois la même erreur de temps en temps, j’ai remarqué que lorsque l’infrastructure est créée et je déploie l’application directement je reçois l’erreur, donc j’ai ajouté la commande « sleep » qui permet d’attendre un moment avant le déploiement de l’application pour que l’infrastructure soit totalement créée.

La troisième difficulté était lors l’utilisation du Runner que j’ai créé, lorsque j’ai voulu utiliser sudo dans certaine commande j’ai reçu une erreur qui m’indique qu’il est nécessaire de lire le mot de passe ou d’utilisé l'option -S pour lire à partir de l'entrée standard mais ce n’est pas une bonne pratique. J’ai fait des recherches et j’ai trouvé une solution qui me permet de supprimer la restriction du mot de passe pour sudo pour l'utilisateur gitlab-runner en ajoutant « gitlab-runner ALL=(ALL) NOPASSWD: ALL » en bas du fichier « sudoers » de ma VM.[ ]

La quatrième difficulté était lors l’utilisation de la commande destroy qui m’affiche un message « Destroy complete! Resources: 0 destroyed. » que aucune ressource n’a été détruit. Cela est à cause de l’abscense du fichier terraform.tfstate qui détermine l'état actuel de l'infrastructure qu'il doit détruire, ce fichier d'état doit se trouver dans le même répertoire que celui où la commande terraform destroy va exécuter c’est pour cette raison que je l’ai déclarer en artéfact. J’ai aussi ajouté l'indicateur -state qui me permet de spécifier le chemin d'accès au fichier d'état que Terraform doit utiliser. [ ]

# Synthèse

Pour conclure, ce projet a été stimulant et enrichissant en termes de technologies et de bonnes pratiques dans le cadre d’intégration et déploiement continue ainsi que déploiement d’infrastructure. Je me suis tout investi à la familiarisation avec les notions GitLab CI ainsi qu’avec Terraform et Ansible. J’ai tout de même enrichi mon bagage en créant un pipeline qui automatise le processus de création d’infrastructure et le déploiement d’application.

Ce projet fut intéressant à réaliser car j’ai renforcé mes connaissances sur GitLab CI en réalisant une chaine d’intégration. J'ai hâte de découvrir le prochain fil rouge.
